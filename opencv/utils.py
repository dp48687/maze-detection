import math
import matplotlib.pyplot as plt


if __name__ == "__main__":
    SIGMOID_1 = lambda x, a, b: 1.0 / (1 + math.exp(-a *(x - b)))

    domain = [i * 0.001 for i in range(-35000, 35000)]

    codomain1 = [(SIGMOID_1(x, 0.5, 0) - SIGMOID_1(x, 0.5, 15)) for x in domain]
    codomain2 = [(SIGMOID_1(x, 1, 0)  - SIGMOID_1(x, 1, 15)) for x in domain]
    codomain3 = [(SIGMOID_1(x, 3, 0) - SIGMOID_1(x, 3, 15)) for x in domain]
    codomain4 = [(SIGMOID_1(x, 10, 0) - SIGMOID_1(x, 10, 15)) for x in domain]

    axes = plt.gca()
    axes.set_xlim((-15, 30))
    axes.set_ylim((-0.1, 1.1))

    plt.plot(domain, codomain1, linewidth=1.4, color="#a3c6ff")
    plt.plot(domain, codomain2, linewidth=1.4, color="#77abff")
    plt.plot(domain, codomain3, linewidth=1.4, color="#3884ff")
    plt.plot(domain, codomain4, linewidth=1.4, color="#0459e2")

    plt.show()
