import os
import os.path as pth

import cv2
import numpy as np


def segmentation(filename):

    if pth.exists(filename):
        fnm, ext = filename.split('.')

        if pth.sep in fnm:
            folder_name = "results"
            if not pth.exists(folder_name):
                os.makedirs(folder_name)
            fnm = fnm.replace(fnm.split(pth.sep)[0], "results")

        img = cv2.imread(filename)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        median_blurred = cv2.medianBlur(gray, 3)
        cv2.imwrite(first_free_filename(fnm + "_median_blurred." + ext), median_blurred)

        averaging_blurred = cv2.blur(img, (5, 5))
        cv2.imwrite(first_free_filename(fnm + "_averaging_blurred." + ext), averaging_blurred)

        thresholded = cv2.adaptiveThreshold(median_blurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 4)
        cv2.imwrite(first_free_filename(fnm + "_thresholded." + ext), thresholded)

        hist_eq = cv2.equalizeHist(thresholded)
        cv2.imwrite(first_free_filename(fnm + "_hist_eq." + ext), hist_eq)

        canny_edges = cv2.Canny(thresholded, 0, 10, apertureSize=3)
        cv2.imwrite(first_free_filename(fnm + "_canny_edges." + ext), canny_edges)

        sobel_edges1 = cv2.Sobel(thresholded, cv2.CV_8U, 1, 0, ksize=5)
        sobel_edges2 = cv2.Sobel(thresholded, cv2.CV_8U, 0, 1, ksize=5)
        cv2.imwrite(first_free_filename(fnm + "_sobel_edges." + ext), sobel_edges1 + sobel_edges2)

        hough_transform(filename, img, sobel_edges1 + sobel_edges2, "sobel")
        hough_transform(filename, img, canny_edges, "canny")

    else:

        raise FileNotFoundError(f"File not found: '{filename}'.")


def hough_transform(filename, original, edges, additional_text):
    fnm, ext = filename.split('.')

    if pth.sep in fnm:
        folder_name = "results"
        if not pth.exists(folder_name):
            os.makedirs(folder_name)
        fnm = fnm.replace(fnm.split(pth.sep)[0], "results")

    min_line_len = 100
    max_line_gap = 5
    lines = cv2.HoughLinesP(edges, 1, np.pi / 200, 10, min_line_len, max_line_gap)

    for i in range(len(lines)):
        for x1, y1, x2, y2 in lines[i]:
            cv2.line(original, (x1, y1), (x2, y2), (0, 255, 0), 2)

    cv2.imwrite(first_free_filename(fnm + f"_segmented_({additional_text})." + ext), original)


def first_free_filename(original):

    if not pth.exists(original):
        return original

    else:
        body, extension = original.split('.')
        cnt = 1
        while pth.exists(f"{body} ({cnt}).{extension}"):
            cnt += 1
        return f"{body} ({cnt}).{extension}"


if __name__ == "__main__":
    segmentation('maze_data/3.jpg')