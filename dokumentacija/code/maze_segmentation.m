
[img, map] = imread('images/5.jpg');
img = rgb2gray(img);


% original image
figure();

subplot(1, 2, 1);
imagesc(img);
title('original');

subplot(1, 2, 2);
imhist(img);
title('histogram');

colormap(gray);



% applying step model
figure();

filtered = step(img, 0.0, 145);

subplot(2, 4, 1);
imagesc(step(img, 0.0, 100));
title('step(0, 100)');

subplot(2, 4, 2);
imagesc(step(img, 0.0, 120));
title('step(0, 120)');

subplot(2, 4, 3);
imagesc(step(img, 0.0, 130));
title('step(0, 130)');

subplot(2, 4, 4);
imagesc(step(img, 0.0, 145));
title('step(0, 145)');

subplot(2, 4, 5);
imagesc(step(img, 0.0, 150));
title('step(0, 150)');

subplot(2, 4, 6);
imagesc(step(img, 0.0, 155));
title('step(0, 155)');

subplot(2, 4, 7);
imagesc(step(img, 0.0, 160));
title('step(0, 160)');

subplot(2, 4, 8);
imagesc(step(img, 0.0, 180));
title('step(0, 180)');

colormap(gray);



% blurring the image
figure();

subplot(1, 2, 1);
imagesc(img);
title('original');

subplot(1, 2, 2);
% blurred = imfilter(img, ones(4)/16);
blurred = conv2(filtered, ones(6)/36, 'same');
imagesc(blurred);
title('blurred image');

colormap(gray);



% histogram equalization
figure();

subplot(1, 4, 1);
imagesc(blurred);
title('blurred image');

subplot(1, 4, 2);
imhist(blurred);
title('histogram');

subplot(1, 4, 3);
equalized = grayslice(filtered/255.0, 140/255);
imagesc(equalized);
title('blurred image');

subplot(1, 4, 4);
[idx, ~] = kmeans(im2double(img(:)), 2);
k_means_img = 1.0 - reshape(idx, size(img));
imagesc(k_means_img);
title('k means (2)');

colormap(gray);



% edge detection
canny_edges = canny(k_means_img);



% edge dilatation
DILATATION_INTENSITY = 4;
figure();

subplot(1, 3, 1);
imagesc(canny_edges);
title('Canny edge detection');

subplot(1, 3, 2);
h_dilated = imdilate(canny_edges, strel('line', DILATATION_INTENSITY, 90));
imagesc(h_dilated);
title('horizontal dilatation');

subplot(1, 3, 3);
v_dilated = imdilate(canny_edges, strel('line', DILATATION_INTENSITY, 0));
imagesc(v_dilated);
title('vertical dilatation');

colormap(gray);



% binarization
figure();

% We do not apply dilatation!
binarized = canny_edges;
imagesc(canny_edges);
title('binarized');

colormap(gray);



% Hough transform
figure();

[H, theta, rho] = hough(binarized);
imagesc(canny(img));

colormap(gray);



% final result
P = houghpeaks(H, 60, 'Threshold', 0.1);
lines = houghlines(binarized, theta, rho, P, 'FillGap',5, 'MinLength', 5);
figure(); hold on;
max_len = 0;
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');

   % Determine the endpoints of the longest line segment
   len = norm(lines(k).point1 - lines(k).point2);
   if ( len > max_len)
      max_len = len;
      xy_long = xy;
   end
end



function res = step(img, shift1, shift2)
    res = img;
    [rows, cols] = size(img);
    for col = 1:cols
        for row = 1:rows
            res(row, col) = limit(img(row, col), shift1, shift2, 255.0);
        end
    end
end

function res = limit(value, lower_bound, upper_bound, other_value)
    if (value < upper_bound) && (value > lower_bound)
        res = value;
    else
        res = other_value;
    end
end

function edges = canny(img)

    edges = edge(img(:,:), 'Canny');

end